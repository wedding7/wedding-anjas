<!DOCTYPE html>
<html lang="id">
   <head>
      <meta charset=UTF-8>
      <meta name=viewport content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="shortcut icon" href="<?=base_url();?>assets/image/favicon.png" >
      <link rel="canonical" href="index.html" >
      <title>Anjas  &amp; Annisaak </title>
      <link rel="stylesheet" href="<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/simplelightbox/1.17.3/simplelightbox.min.css">
      <link rel="stylesheet" href="<?=base_url();?>assets/unpkg.com/aos%402.3.1/dist/aos.css">
      <link href="https://fonts.googleapis.com/css2?family=Tangerine:wght@700&amp;family=Quicksand:wght@500;600;700&amp;family=Italianno&amp;display=swap" rel="stylesheet">
      <link rel="stylesheet" href="<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
      <link href="<?=base_url();?>assets/themes/twelve/stylef700.css?v=1.0.1" rel="stylesheet">
      <link rel="stylesheet" href="<?=base_url();?>assets/themes/style23cd.css?v=1.2.1">
   </head>
   <body>
      <div class="cover">
         <div class="title text-center">
            <div data-aos="fade-up" data-aos-delay="200">
               <img class="mb-3"   width=250>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <h3>Anjas & Annisaak  </h3>
               <h2>- We invite you to join our wedding -</h2>
               <br>
               <button type=button class="btn btn-lg btn-light btn-wedding open_invitation px-5">Buka Undangan</button>
            </div>
         </div>
         <img class="img" src=<?=base_url();?>assets/image/couple4.jpeg>
      </div>
      <section class="header" id="home">
         <div  style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));" class="title text-center">
            <div data-aos="fade-up">
               <h4>Undangan Pernikahan</h4>
               <h1>Anjas prasetyo</h1>
               <h1>&</h1>
               <h1>Annisaak </h1>
               <h2>02 Desember 2021</h2>
            </div>
         </div>
         <img class="img"  src=<?=base_url();?>assets/image/couple3.jpeg>
      </section>
      <div class="story mask_top text-center">
         <section>
            <div class="container" data-aos="fade-up">


         <div class="col-lg-7 mx-auto">

         <img class="mb-5" src=<?=base_url();?>assets/image/flower.png  width=250>
            <div data-aos="fade-up">
               <i>Assalamu’alaikum Warahmatullahi Wabarakatuh</i><br ><br >
               Maha suci Allah SWT yang telah menciptakan makhluk-Nya berpasang-pasangan.<br >
               Dengan ini kami mengundang Bapak/Ibu/Saudara/i<br>
               pada acara pernikahan kami.
               <br >
            </div>
         </div>

            </div>
         </section>
      </div>

      <section class="bridge mask_top-2 mask_bottom" id="couple">

      <div class="d-flex w-100 align-items-center justify-content-center mb-3" data-aos="flip-up" data-aos-delay="200">
            <img src=<?=base_url();?>assets/themes/twelve/images/content/leaves-left.png class="d-none d-lg-block" width=100>
            <h2 class="my-0 mx-5"> Wedding </h2>
            <img src=<?=base_url();?>assets/themes/twelve/images/content/leaves-right.png class="d-none d-lg-block" width=100>
         </div>
         <div class="col-lg-7 mx-auto">
            <div class="row">
               <div data-aos="fade-down-right" data-aos-easing="ease-out-cubic" data-aos-delay="100" data-aos-duration="1000" class="col-lg-6 text-center couples ">
                  <div class="circle-image">
                        <img src=<?=base_url();?>assets/image/1.png alt="Anjas">
                    </div>
                  <h2>
                      Anjas Prasetyo <br>
                  </h2>
                  <p>
                     Putra dari Pasangan <br>
                     <b>Sukotjo &amp; Suciati</b>
                  </p>
               </div>

               <div data-aos="fade-down-left" data-aos-easing="ease-out-cubic" data-aos-duration="1000" class="col-lg-6 text-center couples  order-1 ">
                  <div class="circle-image">
                           <img src=<?=base_url();?>assets/image/2.png alt="Anjas">
                    </div>
                  <h2 style="font-size: 3rem !important;">
                     Annisaak binti yurnalis
                  </h2>

                  <p>
                     Putri dari Pasangan <br>
                     <b>Yurnalis &amp; Nur Hidayati</b>
                  </p>
               </div>
            </div>
         </div>
      </section>
      <section class="location left-flower" id="event">
         <div class="container">
            <img class="mb-5" src=<?=base_url();?>assets/image/flower.png  width=250>
            <div class="row justify-content-center">
               <div class="col-lg-4" data-aos="fade-up" data-aos-delay="300">
                  <div class="card information shadow border-0">
                     <div class="card-body">
                        <h3>Akad Nikah</h3>
                        <p>
                           <b>Kamis, 2 Desember 2021<br>
                           10:00 WIB - 11.00 WIB
                           </b>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4"  data-aos="fade-up" data-aos-delay="400">
                  <div class="card information shadow border-0">
                     <div class="card-body">
                        <h3>Resepsi</h3>
                        <p>
                           <b>Kamis, 2 Desember 2021<br>
                           13:00 WIB - Selesai
                           </b>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <br><br>
            <div class="col-lg-8 mt-5 mx-auto">
               <div class="row">
                  <div class="col-lg-3 col-6 text-center"  data-aos="fade-right" data-aos-delay="100">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="timer"><span class="days">00</span></h4>
                           <p class="m-0"><b>Days</b></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-6 text-center" data-aos="fade-right" data-aos-delay="200">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="timer"><span class="hours">00</span></h4>
                           <p class="m-0"><b>Hours</b></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-6 text-center" data-aos="fade-right" data-aos-delay="300">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="timer"><span class="minutes">00</span></h4>
                           <p class="m-0"><b>Minutes</b></p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-6 text-center" data-aos="fade-right" data-aos-delay="400">
                     <div class="card">
                        <div class="card-body">
                           <h4 class="timer"><span class="seconds">00</span></h4>
                           <p class="m-0"><b>Seconds</b></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <br><br>
            <a target="_blank" href="https://www.google.com/maps/place/1%C2%B003'01.3%22N+103%C2%B058'39.3%22E/@1.0503486,103.9753861,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x9eb0a24904d83acc!8m2!3d1.0503486!4d103.9775748?hl=en" class="btn btn-lg btn-light btn-wedding px-5 mt-4" data-aos="fade-up" data-aos-delay="500">Buka Google Map</a>
            <br><br><br>
         </div>
      </section>
      <section class="gallery mask_top-2 mask_bottom" id="gallery">
         <div class="container">
         <div class="d-flex w-100 align-items-center justify-content-center mb-5">
            <img src=<?=base_url();?>assets/themes/twelve/images/content/leaves-left.png class="d-none d-lg-block" width=100>
            <h2 class="my-0 mx-5">Our Gallery</h2>
            <img src=<?=base_url();?>assets/themes/twelve/images/content/leaves-right.png class="d-none d-lg-block" width=100>
         </div>
         <div class="col-lg-12">
            <div class="row">

            <div class="col-md-6 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="300">
                  <a href="<?=base_url();?>assets/image/couple3.jpeg"><img src=<?=base_url();?>assets/image/couple3.jpeg alt="Gallery 3" class="img-fluid"></a>
               </div>
               <div class="col-md-6 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="400">
                  <a href="<?=base_url();?>assets/image/couple4.jpeg"><img src=<?=base_url();?>assets/image/couple4.jpeg alt="Gallery 4" class="img-fluid"></a>
               </div>

               <div class="col-md-3 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="100">
                  <a href="<?=base_url();?>assets/image/couple1.jpeg"><img src=<?=base_url();?>assets/image/couple1.jpeg alt="Gallery 1" class="img-fluid"></a>
               </div>
               <div class="col-md-3 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="200">
                  <a href="<?=base_url();?>assets/image/couple2.jpeg"><img src=<?=base_url();?>assets/image/couple2.jpeg alt="Gallery 2" class="img-fluid"></a>
               </div>

               <div class="col-md-3 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="500">
                  <a href="<?=base_url();?>assets/image/couple5.jpg"><img src=<?=base_url();?>assets/image/couple5.jpg alt="Gallery 5" class="img-fluid"></a>
               </div>
               <div class="col-md-3 col-6 py-2 px-2 frame" data-aos="zoom-in" data-aos-delay="600">
                  <a href="<?=base_url();?>assets/image/couple6.jpeg"><img src=<?=base_url();?>assets/image/couple6.jpeg alt="Gallery 6" class="img-fluid"></a>
               </div>
            </div>
         </div>
      </section>
      <section class="text-center mt-5">
         <div class="container">
            <img class="m-3" src=<?=base_url();?>assets/image/flower.png width=250>
            <div class="col-md-12 pt-4">
               <p><span>"</span>¨Maha Suci Allah yang telah menciptakan pasangan-pasangan semuanya, baik dari apa yang ditumbuhkan oleh bumi dan dari diri mereka maupun dari apa yang tidak mereka ketahui¡¨<span>"</span></p>
               <h3 class="caption mb-5">Qs. Yaa Siin (36) : 36</h3>
            </div>
         </div>
      </section>
      <section class="guestbook left-flower right-flower" id="guestbook">
         <div class="container">

            <h2 class="my-4">Guest Book</h2>
         </div>
         <div class="container">
            <div class="col-lg-9 mx-auto">
               <div class="card border-0 shadow">
                  <div class="card-body text-left" >
                     <form method="POST" id="formz"  >
                        <div class="form-group">
                           <label for="guestName">Nama</label>
                           <input type=text class="form-control" id="guestName" name=name  placeholder="Your Name"   autocomplete="off">
                        </div>
                        <div class="form-group">
                           <label for="message">Pesan/Doa</label>
                           <textarea class="form-control" id="message" name=message rows="3" required></textarea>
                        </div>
                        <div class="text-center">
                           <button type=submit class="btn btn-light btn-wedding">Kirim  </button>
                        </div>
                     </form>

                     <div id="thanks"  style="display: none; padding-top: 50px;">
                        <center>
                          <h3> Terimakasih atas Pesan & Do'a nya <h3>
                           <img class="mb-5" src=<?=base_url();?>assets/image/flower.png  width=250>
                        </center>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="show-guest-book px-2 mt-5">
            <div class="container text-left shadow">
               <div class="row justify-content-md-center" id="guestBooks">
                  Mengambil data Undangan
               </div>
            </div>
         </div>
      </section>
      <div style="height:50px;width:60px;position: absolute;bottom:0;z-index:-1;visibility: hidden;">
         <audio id="player" autoplay loop>  </audio>
      </div>
        <footer>
            <div class="container">
                  <div class="col-md-12">
                       <small>Design By ZindStudio</small>
                  </div>
            </div>
         </footer>

      <script src=<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js></script>
      <script src=<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js></script>
      <script src=<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/simplelightbox/1.17.3/simple-lightbox.min.js></script>
      <script src=<?=base_url();?>assets/unpkg.com/aos%402.3.1/dist/aos.js></script>
      <link rel="stylesheet" href="<?=base_url();?>assets/cdn.plyr.io/3.6.8/plyr.css" >
      <script src=<?=base_url();?>assets/cdn.plyr.io/3.6.8/plyr.js></script>
      <script src=<?=base_url();?>assets/js/musicv2e209.js?v=1.0.0></script>
      <script src=<?=base_url();?>assets/cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js></script>
      <script>
         AOS.init();
             $('.gallery a').simpleLightbox({
             docClose: false,
             disableScroll: true,
             disableRightClick: true
         });
                     var countDownDate=new Date("02 December 2021 09:00:00").getTime(),x=setInterval(function(){var e=(new Date).getTime(),n=countDownDate-e,t=Math.floor(n/864e5),a=Math.floor(n%864e5/36e5),o=Math.floor(n%36e5/6e4),m=Math.floor(n%6e4/1e3);document.getElementsByClassName("days")[0].innerHTML=t,document.getElementsByClassName("hours")[0].innerHTML=a,document.getElementsByClassName("minutes")[0].innerHTML=o,document.getElementsByClassName("seconds")[0].innerHTML=m,n<0&&(clearInterval(x),document.getElementsByClassName("expired").innerHTML="EXPIRED")},1e3);
      </script>
      <script src=<?=base_url();?>assets/themes/twelve/custom.js></script>
      <script>
         window.addEventListener("contextmenu", function(e) {
             e.preventDefault()
         }, !1);
      </script>
      <script>
         function copyText(element) {
             var range, selection, worked;

             if (document.body.createTextRange) {
                 range = document.body.createTextRange();
                 range.moveToElementText(element);
                 range.select();
             } else if (window.getSelection) {
                 selection = window.getSelection();
                 range = document.createRange();
                 range.selectNodeContents(element);
                 selection.removeAllRanges();
                 selection.addRange(range);
             }

             try {
                 document.execCommand('copy');
                 alert('Number copied');
             }
             catch (err) {
                 alert('Unable to copy number');
             }
         }
      </script>

      <script>
         $( document ).ready(function() {
            getDataGuestBook();
         });


         function getDataGuestBook(){
            $.ajax({
               url: "<?=base_url();?>wedding/getGuestBook",
               cache: false,
               success: function(html){
                  $("#guestBooks").html(html);
               }
            });
         }

         function setThanks(){
            $("#formz").fadeOut( 'fast', function () {
                  $("#thanks") .fadeIn('slow');
               }
            );

         }

         $( "#formz" ).submit(function( event ) {
            event.preventDefault();
            $.ajax({
               url: "<?=base_url();?>wedding/addGuestBook",
               cache: false,
               type : 'POST',
               data : $('#formz').serialize(),
               success: function(html){
                  setThanks();
                  getDataGuestBook();
               }
            });
         });
      </script>
   </body>

</html>