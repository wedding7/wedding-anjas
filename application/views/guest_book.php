
<?php foreach ($data as $key => $value) {  ?>

	<div class="col-md-12 mb-3">
	    <div class="media px-3 media-comment">
	    <img class="rounded-circle mr-3 d-none d-sm-block d-md-block d-lg-block" src="<?= base_url();?>assets/image/user.png" style="max-width: 50px;" alt="Image Avatar">
	    <div class="media-body">
	        <div class="mb-2">
	            <h5 class="h5 mb-0" style="font-weight: bold;"><?= strip_tags($value->nama); ?></h5>
	            <small class="text-muted"><?= strip_tags($value->date); ?>	</small>
	        </div>
	        <p><?= strip_tags($value->pesan); ?>	</p>
	    </div>
	    </div>
	</div>

<?php } ?>
