<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wedding extends CI_Controller
{

    public function index()
    {
        $this->load->view('wedding');
    }

    public function addGuestBook()
    {
        $nama = $this->input->post('name');
        $message = $this->input->post('message');

        if (empty($nama) || empty($message)) {
            echo "{success: false, message:'bad request'}";
            exit;
        }

        $date = date("Y-m-d H:i:s");
        $data = array(
            'date' => $date,
            'nama' => $nama,
            'pesan' => $message,
        );
        $execute = $this->db->insert('guestbook', $data);
        if ($execute) {
            echo "{success: true}";
        } else {
            echo "{success: false}";
        }
    }

    public function getGuestBook()
    {
        $this->db->order_by('date', 'DESC');
        $query = $this->db->get('guestbook');
        $data = $query->result();
        $datas['data'] = $data;
        $this->load->view('guest_book', $datas);
    }
}
